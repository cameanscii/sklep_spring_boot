package com.example.sklep_spring_boot.component;


import com.example.sklep_spring_boot.model.Employee;
import com.example.sklep_spring_boot.model.EmployeeRole;
import com.example.sklep_spring_boot.repository.EmployeeRepository;
import com.example.sklep_spring_boot.repository.EmployeeRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Component
public class InitialDataInitializer implements
        ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private EmployeeRoleRepository employeeRoleRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        addRole("ROLE_ADMIN");
        addRole("ROLE_USER");

        addEmployee("admin", "admin", "ROLE_ADMIN", "ROLE_USER");
        addEmployee("user", "user", "ROLE_USER");
    }

    @Transactional
    public void addRole(String name) {
        Optional<EmployeeRole> employeeRoleOptional = employeeRoleRepository.findByName(name);
        if (!employeeRoleOptional.isPresent()) {
            employeeRoleRepository.save(new EmployeeRole(null, name));
        }
    }

    @Transactional
    public void addEmployee(String username, String password, String... roles) {
        Optional<Employee> employeeOptional = employeeRepository.findByUsername(username);
        if (!employeeOptional.isPresent()) {
            List<EmployeeRole> rolesList = new ArrayList<>();
            for (String role : roles) {
                Optional<EmployeeRole> employeeRoleOptional = employeeRoleRepository.findByName(role);
                employeeRoleOptional.ifPresent(rolesList::add);
            }

            employeeRepository.save(new Employee(
                    null, // id
                    null, // name
                    null, // surname
                    username, // username
                    passwordEncoder.encode(password), // password
                    null, // email
                    new HashSet<EmployeeRole>(rolesList))); // roles
        }
    }
}
