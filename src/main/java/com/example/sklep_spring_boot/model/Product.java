package com.example.sklep_spring_boot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String amount;
    private Double net_price;
    private String storeURL;
    private String store;


    @ManyToOne
    @JsonIgnore
    private Employee owner;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private List<Store> storeList;


}

