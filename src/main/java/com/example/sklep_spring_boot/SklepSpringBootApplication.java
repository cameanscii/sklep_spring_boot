package com.example.sklep_spring_boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SklepSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SklepSpringBootApplication.class, args);
    }
}
