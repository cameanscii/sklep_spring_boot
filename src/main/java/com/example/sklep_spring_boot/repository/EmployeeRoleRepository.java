package com.example.sklep_spring_boot.repository;


import com.example.sklep_spring_boot.model.EmployeeRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeeRoleRepository extends JpaRepository<EmployeeRole, Long> {
    Optional<EmployeeRole> findByName(String name);
}
