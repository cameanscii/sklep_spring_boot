package com.example.sklep_spring_boot.service;

import com.example.sklep_spring_boot.model.Employee;
import com.example.sklep_spring_boot.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private EmployeeService employeeService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Employee> employeeOptional = employeeService.findByUsername(username);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();

            return User
                    .withUsername(employee.getUsername())
                    .password(employee.getPassword())
                    .roles(extractRoles(employee))
                    .build();
        }
        throw new UsernameNotFoundException("User not found by name: " + username);
    }

    private String[] extractRoles(Employee employee) {
        List<String> roles = employee.getRoles().stream().map(role -> role.getName().replace("ROLE_", "")).collect(Collectors.toList());
        String[] rolesArray = new String[roles.size()];
        rolesArray = roles.toArray(rolesArray);

        return rolesArray;
    }

    public Optional<Employee> getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            // nie jesteśmy zalogowani
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return employeeService.findByUsername(user.getUsername());
            // jesteśmy zalogowani, zwracamy user'a
        }

        return Optional.empty();
    }
}