package com.example.sklep_spring_boot.service;


import com.example.sklep_spring_boot.model.Employee;
import com.example.sklep_spring_boot.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;




    public Optional<Employee> findByUsername(String username) {
        return employeeRepository.findByUsername(username);
    }
}
